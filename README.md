# README #

This is the README for the unxsys.com reboot. Original unxsys site was outdated so I upgraded the other site's Bootstrap version to Bootstrap 4.1, and made some other style changes. So this is unxsys' version 2.0 I suppose.

You can see the current site at www.unxsys.com. 

### How do I get set up? ###

Clone the repo and make a branch with whatever changes you want. Send a PR to me and I'll approve things or I'll send 
them back w/ changes.
Note that this repo includes all the Bootstrap 4.1 files and uses SASS CSS, so any style changes would need to be pre-compiled
in order to show up.
No database as this is a fairly straightforward site.

### Contribution guidelines ###
Have at it, just send a PR when finito.

### Who do I talk to? ###
Rachael - that's me! 
Feel free to reach out here or via slack or email.